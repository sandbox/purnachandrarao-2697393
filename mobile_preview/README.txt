# Mobile Preview

Mobile Preview gives content editors an easy way to preview their work on a mobile device while publishing content. 
The preview area is sized to match the pixel dimensions of an iPhone 6.

Utilization:

User use like node/[nodeid]/mobile-preview
Example:hostname/node/1/mobile-preview
