<?php

/**
 * @file
 * Contains \Drupal\siteinfo\SiteInformationController.
 */

namespace Drupal\mobile_preview\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;

/**
 * Controller for Site Information.
 */
class SiteInformationController extends ControllerBase {

  /**
   * Implement siteInformation function.
   */
  public static function siteInformation($first) {
    $content1 = array();
    $allowed_tags = array('iframe', 'div');
	$nid =1;
	$content1['filtered_string'] = array(
		'#markup' => t('<div class="mobile-preview"><iframe src="/node/'.$first.'"></iframe></div>'),
		'#allowed_tags' => $allowed_tags,
		'#attached' => array(
			'library' =>  array(
			  'mobile_preview/block-preview'
			),
		),
	);
  return $content1;
  }
}
